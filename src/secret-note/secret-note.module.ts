import { Module } from '@nestjs/common';
import { SecretNoteService } from './service/secret-note.service';
import { SecretNoteController } from './controller/secret-note.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { SecretNoteEntity } from './models/secretNote.entity';

@Module({
  imports: [TypeOrmModule.forFeature([SecretNoteEntity])],
  providers: [SecretNoteService],
  controllers: [SecretNoteController],
})
export class SecretNoteModule {}
