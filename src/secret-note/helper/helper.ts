import {
  createCipheriv,
  createDecipheriv,
  createHash,
  randomBytes,
} from 'crypto';

const iv = randomBytes(16);
const salt = 'fromOcean';
const hash = createHash('sha1').update(salt);

const key = hash.digest().slice(0, 16);

export const encryptNote = (note) => {
  const cipher = createCipheriv('aes-128-ctr', key, iv);
  let encrypted = cipher.update(note, 'utf8', 'base64');
  encrypted += cipher.final('base64');
  return encrypted;
};

export const decryptNote = (encryptedNote: string) => {
  const decrypt = createDecipheriv('aes-128-ctr', key, iv);
  let text = decrypt.update(encryptedNote, 'base64', 'utf8');
  text += decrypt.final('utf8');
  return text;
};
