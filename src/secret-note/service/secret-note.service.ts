import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { decryptNote, encryptNote } from '../helper/helper';
import { createSecretNoteDTO } from '../models/secretNote.dto';
import { SecretNoteEntity } from '../models/secretNote.entity';
import { ISecretNote } from '../models/secretNote.interface';

@Injectable()
export class SecretNoteService {
  constructor(
    @InjectRepository(SecretNoteEntity)
    private noteRepository: Repository<SecretNoteEntity>,
  ) {}

  async add(request: createSecretNoteDTO): Promise<ISecretNote> {
    return this.noteRepository.save({
      note_encrypted: encryptNote(request.note),
    });
  }

  async getById(id: number, encoded: boolean): Promise<string> {
    const encryptedSecretNote = await (
      await this.noteRepository.findOneBy({ id })
    ).note_encrypted;

    return encoded ? encryptedSecretNote : decryptNote(encryptedSecretNote);
  }
}
