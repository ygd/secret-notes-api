import { Body, Controller, Get, Param, Post, Query } from '@nestjs/common';
import { createSecretNoteDTO } from '../models/secretNote.dto';
import { ISecretNote } from '../models/secretNote.interface';
import { SecretNoteService } from '../service/secret-note.service';

@Controller('secret-notes')
export class SecretNoteController {
  constructor(private secretNoteService: SecretNoteService) {}

  @Post()
  add(@Body() note: createSecretNoteDTO): Promise<ISecretNote> {
    return this.secretNoteService.add(note);
  }
  @Get(':id')
  findOne(
    @Param('id') id: number,
    @Query('encoded') encoded: boolean,
  ): Promise<string> {
    return this.secretNoteService.getById(id, encoded);
  }
}
