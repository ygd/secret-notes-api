import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class SecretNoteEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  note_encrypted: string;
}
