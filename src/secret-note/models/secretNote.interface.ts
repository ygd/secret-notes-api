export interface ISecretNote {
  id: number;
  note_encrypted: string;
}
